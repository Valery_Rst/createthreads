package net.manyThreads;

public class MainDemo {
    public static void main(String[] args) {
        byte counter = 10;
        for (int i = 0; i < counter; i++) {
            new TemplateThread().start();
        }
    }
}
