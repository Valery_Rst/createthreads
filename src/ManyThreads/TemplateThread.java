package net.manyThreads;

public class TemplateThread extends Thread {
    public void run() {
        System.out.printf("From %s  \n", Thread.currentThread().getName());
        try{
            Thread.sleep(500);
        }
        catch(InterruptedException e){
            System.out.println("Thread has been interrupted");
        }
    }
}
